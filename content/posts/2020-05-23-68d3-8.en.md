---
title: African Officials Respond to France’s Restitution Report
link: https://www.nytimes.com/2018/11/30/arts/design/africa-macron-report-restitution.html
image: https://static01.nyt.com/images/2018/12/01/arts/01senegal-item-1/01senegal-item-1-mediumThreeByTwo210.jpg?quality=100&auto=webp
categories: ["nytimes"]
date: 2018-11-30
keywords:
- Côte d'Ivoire
- Ivory Coast
- News
- Actualité
- Titrologie
- Titrologue
tags:
- Politics
filename: 2020-05-23-68d3-8.md
description: African Officials Respond to France’s Restitution Report
type: post

---
