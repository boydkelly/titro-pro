+++
author = "=categories"
categories = ["france24"]
date = 2020-05-31T07:07:54Z
description = "Ivory Coast's 'rebel city' of Bouaké looks to the future"
filename = ""
image = "https://s.france24.com/media/display/92b1e836-a0d8-11ea-9555-005056a98db9/w:310/p:16x9/XXBT%20BIL%20BOUAKE%20PUSH%20PICTURE%20%280-00-00-00%29.jpg"
link = "https://www.france24.com/en/africa/20200529-revisited-ivory-coast-s-rebel-city-of-bouak%C3%A9-looks-to-the-future"
tags = ["history", "economy"]
title = "Ivory Coast's 'rebel city' of Bouaké looks to the future"
type = "post"

+++
