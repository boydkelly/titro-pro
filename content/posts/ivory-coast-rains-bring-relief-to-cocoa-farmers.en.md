---
date: 2020-06-03T11:33:15+00:00
author: "=categories"
type: post
title: Ivory Coast rains bring relief to cocoa farmers
link: https://af.reuters.com/article/investingNews/idAFKBN2390U1-OZABS
image: https://en.wikipedia.org/wiki/File:Cacao-pod-k4636-14.jpg
tags:
- economy
categories:
- reuters
description: Ivory Coast rains bring relief to cocoa farmers
filename: ''

---
