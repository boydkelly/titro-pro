---
title: Le gouvernement entérine la fin du franc CFA dans un projet de loi
link: https://www.ouest-france.fr/monde/afrique/le-gouvernement-enterine-la-fin-du-franc-cfa-dans-un-projet-de-loi-6841246
image: https://media.ouest-france.fr/v1/pictures/MjAyMDA1OGRhNTBiMmYwMzQ2NWIzMTMzOWI3MjE3NGRiZmQyNTM?width=940&focuspoint=50%2C25&cropresize=1&client_id=bpeditorial&sign=9e90478752c63d59653a817d0360688bb9c990b72c6843af41e291db9a9766b2
categories: ["ouest-france"] 
date: 2020-05-23T08:12:10.000+00:00
keywords:
- Économie
- Côte d'Ivoire
- Ivory Coast
- News
- Actualité
- Titrologie
- Titrologue
tags:
- Économie
filename: 2020-05-23-725c-2.fr.md
description: Le gouvernement entérine la fin du franc CFA dans un projet de loi
type: post

---
