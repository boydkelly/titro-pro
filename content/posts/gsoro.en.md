---
title: Former Ivory Coast rebel leader Guillaume Soro fined $7m in absentia
date: 2020-05-22T00:11:20.000+00:00
link: https://www.bbc.com/news/world-africa-52457131
image: https://ichef.bbci.co.uk/news/660/cpsprodpb/9CDF/production/_87095104_guilliaumeafp.jpg
type: post
categories: ["bbc"] 
description: Former Ivory Coast rebel leader Guillaume Soro fined $7m in absentia
keywords:
- key
- words
lang: english
tags:
- politics

---
