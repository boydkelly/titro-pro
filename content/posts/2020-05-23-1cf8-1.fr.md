---
title: "Procès de Soro Guillaume ce mardi en Côte d'Ivoire"
link: "https://www.bbc.com/afrique/region-52450716"
image: "https://ichef.bbci.co.uk/news/660/cpsprodpb/154E5/production/_111996278_sosoos.jpg"
categories: ["bbc"]
date: 2020-05-23T08:12:10+00:00
keywords: ["Côte d'Ivoire", "Ivory Coast", "News", "Actualité", "Titrologie", "Titrologue"]
tags: ["politique"]
filename: 2020-05-23-1cf8-1.fr.md
description: "Procès de Soro Guillaume ce mardi en Côte d'Ivoire"
type: post
draft: false
---
