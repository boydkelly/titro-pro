#!/usr/bin/bash
[[ "$1" = "dev" || "$1" = "prod" ]] ||  { echo "Note: need dev or prod"; exit 1 ; }

[[ "$1" = "dev" ]] && DRAFT=true
[[ "$1" = "prod" ]] && DRAFT=false

DEST=~/dev/titro-pro/content/posts/

[ -d $DEST ] || { echo "directory not found" ; exit 1 ; }

for x in `ls *.md`; do
  sed -i "s/draft:.*$/draft: $DRAFT/g" $x
done

[[ $DRAFT = "true" ]] && { cp -v *.md $DEST ; hugo -D server ; }
[[ $DRAFT = "false" ]] && { mv -v *.md $DEST ; git add content/posts/* && git commit -a -m "Posts published on "`date -Im` && git push origin master ; }

